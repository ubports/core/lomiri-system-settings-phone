# Gaelic; Scottish translation for lomiri-system-settings
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-phone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
# Michael Bauer <fios@akerbeltz.org>, 2014.
# GunChleoc <fios@foramnagaidhlig.net>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-phone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 16:30+0000\n"
"PO-Revision-Date: 2014-10-20 19:38+0000\n"
"Last-Translator: GunChleoc <Unknown>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#. TRANSLATORS: This string will be truncated on smaller displays.
#: ../plugins/phone/CallForwardItem.qml:157
#: ../plugins/phone/CallForwardItem.qml:202
msgid "Forward to"
msgstr "Sìn air adhart gu"

#: ../plugins/phone/CallForwardItem.qml:171
#, fuzzy
msgid "Enter a number"
msgstr "Àireamh fòn"

#: ../plugins/phone/CallForwardItem.qml:218
msgid "Call forwarding can’t be changed right now."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:45 ../plugins/phone/MultiSim.qml:54
#: ../plugins/phone/NoSims.qml:28 ../plugins/phone/SingleSim.qml:42
msgid "Call forwarding"
msgstr "Sìneadh air adhart ghairmean"

#: ../plugins/phone/CallForwarding.qml:131
msgid "Forward every incoming call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:146
msgid "Redirects all phone calls to another number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:158
msgid "Call forwarding status can’t be checked "
msgstr ""

#: ../plugins/phone/CallForwarding.qml:166
#, fuzzy
msgid "Forward incoming calls when:"
msgstr "Airson gairmean a-mach, cleachd:"

#: ../plugins/phone/CallForwarding.qml:175
msgid "I’m on another call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:186
msgid "I don’t answer"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:197
#, fuzzy
msgid "My phone is unreachable"
msgstr "Chan eil ModemManager ri làimh"

#: ../plugins/phone/CallForwarding.qml:227
msgid "Contacts…"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:240
msgid "Cancel"
msgstr "Sguir dheth"

#: ../plugins/phone/CallForwarding.qml:253
msgid "Set"
msgstr "Suidhich"

#: ../plugins/phone/CallForwarding.qml:274
#, fuzzy
msgid "Please select a phone number"
msgstr "Àireamh fòn"

#: ../plugins/phone/CallForwarding.qml:283
#, fuzzy
msgid "Numbers"
msgstr "àireamhan"

#: ../plugins/phone/CallForwarding.qml:302
msgid "Could not forward to this contact"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:303
msgid "Contact not associated with any phone number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:305
msgid "OK"
msgstr "Ceart ma-thà"

#: ../plugins/phone/CallForwarding.qml:382
#, fuzzy
msgid "All calls"
msgstr "gairm"

#: ../plugins/phone/CallForwarding.qml:384
#, fuzzy
msgid "Some calls"
msgstr "Gairmean fòn:"

#: ../plugins/phone/CallForwarding.qml:386
msgid "Off"
msgstr "Dheth"

#: ../plugins/phone/CallWaiting.qml:31 ../plugins/phone/CallWaiting.qml:86
#: ../plugins/phone/MultiSim.qml:42 ../plugins/phone/NoSims.qml:34
#: ../plugins/phone/SingleSim.qml:34
msgid "Call waiting"
msgstr "Tha gairm a' feitheamh"

#: ../plugins/phone/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""
"Bheir seo comas dhut gairm ùr a fhreagairt no a thòiseachadh fhad 's a bhios "
"tè eile agad agus leum a ghearradh eadar an dà dhiubh."

#: ../plugins/phone/MultiSim.qml:67 ../plugins/phone/NoSims.qml:42
msgid "Services"
msgstr "Seirbheisean"

#: ../plugins/phone/PageComponent.qml:32
msgid "Phone"
msgstr "Fòn"

#: ../plugins/phone/PageComponent.qml:102
#, fuzzy
msgid "Dialpad tones"
msgstr "Fuaimean a' phada-daithealaidh"

#: ../plugins/phone/ServiceInfo.qml:109
#, qt-format
msgid "Last called %1"
msgstr "Gairm mu dheireadh %1"

#: ../plugins/phone/ServiceInfo.qml:119
msgid "Call"
msgstr "Gairm"

#. TRANSLATORS: %1 is the name of the (network) carrier
#: ../plugins/phone/Services.qml:36 ../plugins/phone/SingleSim.qml:55
#, qt-format
msgid "%1 Services"
msgstr "Seirbheisean %1"

#: ../plugins/phone/SingleSim.qml:29
msgid "SIM"
msgstr "SIM"

#~ msgid "phone"
#~ msgstr "phone;fòn"

#~ msgid "services"
#~ msgstr "seirbheisean"

#~ msgid "forwarding"
#~ msgstr "sìneadh air adhart"

#~ msgid "waiting"
#~ msgstr "feitheamh"

#~ msgid "call"
#~ msgstr "gairm"

#~ msgid "dialpad"
#~ msgstr "dialpad;daitheal;pada-deitheil;àireamhan"

#~ msgid "shortcuts"
#~ msgstr "ath-ghoiridean"

#~ msgid "numbers"
#~ msgstr "àireamhan"
