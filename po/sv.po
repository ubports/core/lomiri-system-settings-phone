# Swedish translation for lomiri-system-settings-phone (Touch interface)
# Copyright © 2013-2014 Rosetta Contributors and Canonical Ltd 2013-2014
# This file is distributed under the same license as the lomiri-system-settings-phone package.
# Josef Andersson <josef.andersson@fripost.org>, 2014.
# clone <josef.andersson@fripost.org>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-phone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 16:30+0000\n"
"PO-Revision-Date: 2023-02-27 18:39+0000\n"
"Last-Translator: Luna Jernberg <droidbittin@gmail.com>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/lomiri/lomiri-"
"system-settings-phone/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2015-07-16 05:41+0000\n"

#. TRANSLATORS: This string will be truncated on smaller displays.
#: ../plugins/phone/CallForwardItem.qml:157
#: ../plugins/phone/CallForwardItem.qml:202
msgid "Forward to"
msgstr "Vidarekoppla till"

#: ../plugins/phone/CallForwardItem.qml:171
msgid "Enter a number"
msgstr "Ange nummer"

#: ../plugins/phone/CallForwardItem.qml:218
msgid "Call forwarding can’t be changed right now."
msgstr "Samtalsvidarebefordring kan inte ändras just nu."

#: ../plugins/phone/CallForwarding.qml:45 ../plugins/phone/MultiSim.qml:54
#: ../plugins/phone/NoSims.qml:28 ../plugins/phone/SingleSim.qml:42
msgid "Call forwarding"
msgstr "Vidarekoppling"

#: ../plugins/phone/CallForwarding.qml:131
msgid "Forward every incoming call"
msgstr "Vidarebefodra varje inkommande samtal"

#: ../plugins/phone/CallForwarding.qml:146
msgid "Redirects all phone calls to another number."
msgstr "Vidarebefodrar alla samtal till ett annat nummer."

#: ../plugins/phone/CallForwarding.qml:158
msgid "Call forwarding status can’t be checked "
msgstr "Status för vidarebefordran av samtal kan inte kontrolleras "

#: ../plugins/phone/CallForwarding.qml:166
msgid "Forward incoming calls when:"
msgstr "Vidarebefodra inkommande samtal när:"

#: ../plugins/phone/CallForwarding.qml:175
msgid "I’m on another call"
msgstr "Jag är i ett annat samtal"

#: ../plugins/phone/CallForwarding.qml:186
msgid "I don’t answer"
msgstr "Jag svarar inte"

#: ../plugins/phone/CallForwarding.qml:197
msgid "My phone is unreachable"
msgstr "Min telefon är otillgänlig"

#: ../plugins/phone/CallForwarding.qml:227
msgid "Contacts…"
msgstr "Kontakter…"

#: ../plugins/phone/CallForwarding.qml:240
msgid "Cancel"
msgstr "Avbryt"

#: ../plugins/phone/CallForwarding.qml:253
msgid "Set"
msgstr "Ställ in"

#: ../plugins/phone/CallForwarding.qml:274
msgid "Please select a phone number"
msgstr "Snälla välj ett telefonnummer"

#: ../plugins/phone/CallForwarding.qml:283
msgid "Numbers"
msgstr "Nummer"

#: ../plugins/phone/CallForwarding.qml:302
msgid "Could not forward to this contact"
msgstr "Kunde inte vidarebefodra till denna kontakt"

#: ../plugins/phone/CallForwarding.qml:303
msgid "Contact not associated with any phone number."
msgstr "Kontakt som inte är associerad med något telefonnummer."

#: ../plugins/phone/CallForwarding.qml:305
msgid "OK"
msgstr "OK"

#: ../plugins/phone/CallForwarding.qml:382
msgid "All calls"
msgstr "Alla samtal"

#: ../plugins/phone/CallForwarding.qml:384
msgid "Some calls"
msgstr "Vissa samtal"

#: ../plugins/phone/CallForwarding.qml:386
msgid "Off"
msgstr "Av"

#: ../plugins/phone/CallWaiting.qml:31 ../plugins/phone/CallWaiting.qml:86
#: ../plugins/phone/MultiSim.qml:42 ../plugins/phone/NoSims.qml:34
#: ../plugins/phone/SingleSim.qml:34
msgid "Call waiting"
msgstr "Väntande samtal"

#: ../plugins/phone/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""
"Låter dig svara eller starta ett nytt samtal under tiden du är i ett annat "
"samtal, samt växla mellan dem"

#: ../plugins/phone/MultiSim.qml:67 ../plugins/phone/NoSims.qml:42
msgid "Services"
msgstr "Tjänster"

#: ../plugins/phone/PageComponent.qml:32
msgid "Phone"
msgstr "Telefon"

#: ../plugins/phone/PageComponent.qml:102
msgid "Dialpad tones"
msgstr "Knappsats toner"

#: ../plugins/phone/ServiceInfo.qml:109
#, qt-format
msgid "Last called %1"
msgstr "Senast uppringd %1"

#: ../plugins/phone/ServiceInfo.qml:119
msgid "Call"
msgstr "Samtal"

#. TRANSLATORS: %1 is the name of the (network) carrier
#: ../plugins/phone/Services.qml:36 ../plugins/phone/SingleSim.qml:55
#, qt-format
msgid "%1 Services"
msgstr "%1-tjänster"

#: ../plugins/phone/SingleSim.qml:29
msgid "SIM"
msgstr "SIM"

#~ msgid "phone"
#~ msgstr "telefon"

#~ msgid "services"
#~ msgstr "tjänster"

#~ msgid "forwarding"
#~ msgstr "vidarekoppla"

#~ msgid "waiting"
#~ msgstr "väntar"

#~ msgid "call"
#~ msgstr "ring upp"

#~ msgid "dialpad"
#~ msgstr "knappsats"

#~ msgid "shortcuts"
#~ msgstr "genvägar"

#~ msgid "numbers"
#~ msgstr "nummer"
